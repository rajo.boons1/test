export default {
  user: {
    token: sessionStorage.getItem('TOKEN'),
    data: {}
  },
  products: {
    loading: false,
    data: [],
    links: [],
    from: null,
    to: null,
    page: 1,
    limit: null,
    total: null
  },
  users: {
    loading: false,
    data: [],
    links: [],
    from: null,
    to: null,
    page: 1,
    limit: null,
    total: null
  },
  customers: {
    loading: false,
    data: [],
    links: [],
    from: null,
    to: null,
    page: 1,
    limit: null,
    total: null
  },
  countries: [],
  orders: {
    loading: false,
    data: [],
    links: [],
    from: null,
    to: null,
    page: 1,
    limit: null,
    total: null
  },
  toast: {
    show: false,
    message: '',
    delay: 5000
  },
  dateOptions: [
    {key: '1d', text: 'Hier'},
    {key: '1k', text: 'Semaine dernière'},
    {key: '2k', text: '2 Dernières semaines'},
    {key: '1m', text: 'Le Mois Dernier'},
    {key: '3m', text: '3 Derniers Mois'},
    {key: '6m', text: '6 Derniers Mois'},
    {key: 'all', text: 'Tous les rapports'},
  ]
}
